/*
 *
 */
package com.fpt.logminer.impl;

import com.fpt.logminer.database.DatabaseClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.fpt.logminer.api.LogMiner.LOG_TYPE;

/**
 * @author Son Phan
 */
public class CachedRedoLog extends RedoLog {
    private static final Logger       log           = LoggerFactory.getLogger(CachedRedoLog.class);
    private              List<String> logList       = null;
    private              int          logCount      = 0;
    private              int          index         = 0;
    private              int          queryPageSize = 10;

    public CachedRedoLog(DatabaseClient dbClient) {
        super(dbClient);
    }

    public void setPageSize(int page) {
        queryPageSize = page;
    }

    RedoLogQuery getQuery() {
        return new RedoLogQueryImpl();
    }

    private class RedoLogQueryImpl implements RedoLogQuery {

        @Override
        public boolean hasNext() {
            if (logCount == 0) {
                logCount = getLogCount();
            }
            if (logList == null) {
                try {
                    logList = logType == LOG_TYPE.ONLINE ? getOnlineFileList() : getOfflineFileList();
                } catch (Exception ex) {
                    log.error("Fail to get redo log list. Error message:%s" + ex.getMessage());
                }
                return CachedRedoLog.this.index < CachedRedoLog.this.logCount;
            }

            return index < logCount;
        }

        @Override
        public List<String> next() {
            int last = index + queryPageSize;
            if (last > logCount) {
                last = logCount;
            }
            List<String> list = logList.subList(index, last);
            index += last;

            return list;
        }
    }
}
