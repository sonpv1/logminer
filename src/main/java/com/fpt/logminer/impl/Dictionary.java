/*
 *
 */
package com.fpt.logminer.impl;

import com.fpt.logminer.Constants;
import com.fpt.logminer.config.Config;
import com.fpt.logminer.database.DatabaseClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;


public class Dictionary implements Constants {

    private final Logger         log                    = LoggerFactory.getLogger(Dictionary.class);
    private       String         SQL_CREATE_DIECTIONARY = null; // for example "BEGIN dbms_logmnr_d.build(dictionary_filename => 'dictionary.ora', dictionary_location =>'" + DATABASE_DICTIONARY_PATH + "'); END;";
    private       DatabaseClient dbClient               = null;
    private       Config         config                 = null;

    public Dictionary(DatabaseClient dbClient) {
        this.dbClient = dbClient;
    }

    public Dictionary(DatabaseClient dbClient, Config config) {
        this.dbClient = dbClient;
        this.config = config;
    }

    public void build() throws Exception {
        String path = getDictionaryPath();
        String fileName = getDictionaryFileName();
        SQL_CREATE_DIECTIONARY = "BEGIN dbms_logmnr_d.build(dictionary_filename => '" + fileName + "', dictionary_location =>'" + path + "'); END;";
        if (log.isDebugEnabled()) {
            log.debug(String.format("Dictionary build options: %s", SQL_CREATE_DIECTIONARY));
        }
        dbClient.execute(SQL_CREATE_DIECTIONARY);
    }

    public String getDictionaryPath() throws Exception {
        if (null != config.getDictionaryPath()) {
            return config.getDictionaryPath();
        }
        try (ResultSet resultSet = dbClient.executeGetResult(SQL_QUERY_DIRECTORY_PATH)) {
            if (!resultSet.next()) {
                throw new RuntimeException("Dictionary path has not set.");
            }
            return resultSet.getString(1);
        } finally {
            dbClient.releaseStatement();
        }
    }

    /**
     * To get dictionary file name from configuration
     * @return
     */
    public String getDictionaryFileName() {
        return null == config.getDictionaryFileName() ? DICTIONARY_FILE : config.getDictionaryFileName();
    }
}
