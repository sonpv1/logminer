/*
 *
 */
package com.fpt.logminer.impl;

import java.util.List;

public interface RedoLogQuery {

    boolean hasNext();

    List<String> next();
}
