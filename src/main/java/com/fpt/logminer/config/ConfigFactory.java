package com.fpt.logminer.config;

import com.fpt.logminer.api.LogMiner;

/**
 * Created by Son Phan on 4/3/2017.
 */
public class ConfigFactory {

    public static Config createDefaultConfig() {
        Config config = new Config();
        config.setBuildType(LogMiner.LOG_TYPE.ONLINE);
        config.setDictType(LogMiner.DICT_TYPE.ONLINE);
        config.setCommittedDataOnly(true);
        config.setNoRowid(true);
        config.setStartSCN("0");

        return config;
    }
}

