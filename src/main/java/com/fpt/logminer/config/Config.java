package com.fpt.logminer.config;

import com.fpt.logminer.impl.DefaultLogMiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import static com.fpt.logminer.api.LogMiner.DICT_TYPE;
import static com.fpt.logminer.api.LogMiner.LOG_TYPE;


public class Config {
    private final Logger    log                   = LoggerFactory.getLogger(Config.class);
    private       DICT_TYPE dict_type             = DICT_TYPE.ONLINE;
    private       LOG_TYPE  build_type            = LOG_TYPE.ONLINE;
    private       boolean   isCommittedDataOnly   = true;
    private       boolean   isNoRowid             = true;
    private       String    startSCN              = "0";
    private       String    endSCN                = null;
    private       boolean   isSupplementalLogMode = false;
    private       boolean   isNoSQLDelimiter      = true;
    private       boolean   isContinuousMine      = false;
    private boolean   isSkipCorruption      = true;
    private int       pageSize              = 1000;
    private boolean   isDDLTracking         = false;   // Database must be opened if true
    private boolean   isOfflineDatabase     = false;
    private boolean   hasDictionary         = false;
    private String dictionaryPath;
    private String dictionaryFileName;

    public boolean hasDictionary() {
        return hasDictionary;
    }

    public void setHasDictionary(boolean hasDictionary) {
        this.hasDictionary = hasDictionary;
    }

    public String getDictionaryPath() {
        return dictionaryPath;
    }

    public void setDictionaryPath(String dictionaryPath) {
        this.dictionaryPath = dictionaryPath;
    }

    public String getDictionaryFileName() {
        return dictionaryFileName;
    }

    public void setDictionaryFileName(String dictionaryFileName) {
        this.dictionaryFileName = dictionaryFileName;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int size) {
        this.pageSize = size;
    }

    public void setOfflineDatabase(boolean isOffline) {
        isOfflineDatabase = isOffline;
    }

    public DICT_TYPE getDictType() {
        return dict_type;
    }

    public void setDictType(DICT_TYPE dictType) {
        this.dict_type = dictType;
    }

    public LOG_TYPE getLogType() {
        return build_type;
    }

    public void setEnabledSupplementalLog() {
        isSupplementalLogMode = true;
    }

    public boolean isSupplementalLogEnabled() {
        return isSupplementalLogMode;
    }

    public void setBuildType(LOG_TYPE buildType) {
        this.build_type = buildType;
    }

    public void setNoRowid(boolean isNoRowid) {
        this.isNoRowid = isNoRowid;
    }

    public void setNoSQLDelimiter(boolean isNoDelimiter) {
        this.isNoSQLDelimiter = isNoDelimiter;
    }

    public void setContinuousMine(boolean isContinuousMine) {
        this.isContinuousMine = isContinuousMine;
    }

    public void setCommittedDataOnly(boolean isCommittedOnly) {
        isCommittedDataOnly = isCommittedOnly;
    }

    public void setStartSCN(String scn) {
        try {
            Double.parseDouble(scn);
            this.startSCN = scn;
        } catch (Exception ex) {
            System.out.println(String.format("Not a valid SCN: %s, ignore parameter", scn));
        }
    }

    public void setEndSCN(String scn) {
        try {
            Double.parseDouble(scn);
            this.endSCN = scn;
        } catch (Exception ex) {
            System.out.println(String.format("Not a valid SCN: %s, ignore parameter", scn));
        }
    }

    public String buildParameters() {
        isDDLTracking = dict_type != DICT_TYPE.ONLINE && !isOfflineDatabase;

        StringBuilder parameter = new StringBuilder();
        if (startSCN != null) {
            parameter.append(String.format("STARTSCN =>%s", startSCN));
        }
        if (endSCN != null) {
            parameter.append(String.format(",ENDSCN =>%s", endSCN));
        }

        if (dict_type == DICT_TYPE.STORE_IN_FILE) {
            parameter.append(", DICTFILENAME => '%s" + File.separator + "%s'");
        }

        StringBuilder option = new StringBuilder();
        if (dict_type == DICT_TYPE.ONLINE) {
            option.append("DBMS_LOGMNR.DICT_FROM_ONLINE_CATALOG");
        } else if (dict_type == DICT_TYPE.STORE_IN_REDO_LOG) {
            option.append("DBMS_LOGMNR.DICT_FROM_REDO_LOGS");
        }

        // Optional 
//        if (isContinuousMine) {
//            if (option.length() > 0) {
//                option.append(" + ");
//            }
//            option.append("DBMS_LOGMNR.CONTINUOUS_MINE");
//        }

        if (isNoSQLDelimiter) {
            if (option.length() > 0) {
                option.append(" + ");
            }
            option.append("DBMS_LOGMNR.NO_SQL_DELIMITER");
        }

        if (isCommittedDataOnly) {
            if (option.length() > 0) {
                option.append(" + ");
            }
            option.append("DBMS_LOGMNR.COMMITTED_DATA_ONLY");
        }
        if (isNoRowid) {
            if (option.length() > 0) {
                option.append(" + ");
            }
            option.append("DBMS_LOGMNR.NO_ROWID_IN_STMT");
        }
        if (isSkipCorruption) {
            if (option.length() > 0) {
                option.append(" + ");
            }
            option.append("DBMS_LOGMNR.SKIP_CORRUPTION");
        }
        if (startSCN != null && endSCN != null) {
            if (option.length() > 0) {
                option.append(" + ");
            }
            option.append("DBMS_LOGMNR.CONTINUOUS_MINE");
        }
        if (isDDLTracking) {
            if (option.length() > 0) {
                option.append(" + ");
            }
            option.append("DBMS_LOGMNR.DDL_DICT_TRACKING");
        }

        if (option.length() > 0) {
            parameter.append(", OPTIONS => ").append(option);
        }

        if (log.isDebugEnabled()) {
            log.debug(String.format("Config parameters: %s", parameter.toString()));
        }
        return parameter.toString();
    }
}
