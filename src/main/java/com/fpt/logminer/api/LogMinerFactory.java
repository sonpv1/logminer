package com.fpt.logminer.api;

import com.fpt.logminer.config.Config;
import com.fpt.logminer.database.DatabaseClient;
import com.fpt.logminer.impl.DefaultLogMiner;

/**
 * Created by Son Phan on 4/3/2017.
 */
public class LogMinerFactory {
    public static LogMiner createLogMiner(DatabaseClient db, Config config) {
        return new DefaultLogMiner(db, config);
    }
}
