package com.fpt.logminer.api;

import java.sql.ResultSet;

/**
 * Created by Son Phan on 4/3/2017.
 */
public interface LogMiner {

    public void buildDictionary();

    public void build();

    boolean hasNextBuild();

    public void start();

    public void end();

    public ResultSet query(String sql);

    public void sync(RedoValue value) throws Exception;

    // Indicate using online dictionary or store dictionary into log/file
    public static enum DICT_TYPE {
        ONLINE,
        STORE_IN_REDO_LOG, // used for OFFLINE redo log only
        STORE_IN_FILE
    }

    // Indicate to build online log or archived log files
    public static enum LOG_TYPE {
        ONLINE, // conflict with STORE_IN_REDO_LOG
        OFFLINE
    }

    public static enum OPERATION {
        UPDATE,
        DELETE,
        INSERT,
        DDL
    }
}