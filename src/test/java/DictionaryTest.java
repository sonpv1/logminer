import com.fpt.logminer.config.Config;
import com.fpt.logminer.impl.Dictionary;
import org.junit.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.*;


public class DictionaryTest extends RapSyncTestBase {

    @Test
    public void testDictionary() throws Exception {
        assertTrue(sourceDatabase != null);
        Config config = new Config();
        config.setDictionaryPath("D:\\app");

        Dictionary dict = new Dictionary(sourceDatabase, config);
        try {
            if(!dict.getDictionaryPath().isEmpty() && !Files.exists(Paths.get(dict.getDictionaryPath() + File.separator + dict.getDictionaryFileName()))){
                dict.build();
            }
        } catch (Exception ex) {
            System.out.println("Fail to create dictionary file. Error message:" );
            System.out.println(ex.getMessage());
        }
    }
}
