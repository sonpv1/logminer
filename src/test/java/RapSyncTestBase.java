import com.fpt.logminer.database.DatabaseClient;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RapSyncTestBase {

    protected final Logger log = LoggerFactory.getLogger(RapSyncTestBase.class);

    protected static final String DATABASE_DRIVER_VAL = "file:///D:/app/ojdbc6.jar";
    protected static final String DATABASE_SOURCE_URL_VAL = "jdbc:oracle:thin:@127.0.0.1:1521:orcl";
    protected static final String DATABASE_SOURCE_USERNAME_VAL = "SYSTEM";
    protected static final String DATABASE_SOURCE_PASSWORD_VAL = "123456";
    protected static final String DATABASE_DESTINATION_URL_VAL = "jdbc:oracle:thin:@127.0.0.1:1521:orcl";
    protected static final String DATABASE_DESTINATION_USERNAME_VAL = "SYSTEM";
    protected static final String DATABASE_DESTINATION_PASSWORD_VAL = "123456";
    protected DatabaseClient sourceDatabase = null;
    protected DatabaseClient destinationDatabase = null;

    @Before
    public void setUp() throws Exception {
        sourceDatabase = new DatabaseClient(DATABASE_DRIVER_VAL, DATABASE_SOURCE_URL_VAL, DATABASE_SOURCE_USERNAME_VAL, DATABASE_SOURCE_PASSWORD_VAL);
        destinationDatabase = new DatabaseClient(DATABASE_DRIVER_VAL, DATABASE_DESTINATION_URL_VAL, DATABASE_DESTINATION_USERNAME_VAL, DATABASE_DESTINATION_PASSWORD_VAL);
    }

    @After
    public void tearDown() {
        sourceDatabase.release();
        destinationDatabase.release();
    }
}
