import com.fpt.logminer.api.LogMiner;
import com.fpt.logminer.api.LogMinerFactory;
import com.fpt.logminer.config.Config;
import com.fpt.logminer.impl.RedoLog;
import org.junit.Test;

import java.sql.ResultSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;

public class LogMinerTest extends RapSyncTestBase {

    @Test
    public void testStoreInFileLogMinerRead() {
        RedoLog redoLog = new RedoLog(sourceDatabase);
        int lastSCN    = 2301198;
        int nextSCN    = lastSCN;
        while (true) {
            int currentSCN = Integer.valueOf(redoLog.getCurrentSCN());

            if (currentSCN - lastSCN > 1000) {
                while (currentSCN - lastSCN > 0) {
                    nextSCN += 1000;
                    if (nextSCN > currentSCN) {
                        nextSCN = currentSCN;
                    }
                    System.out.println("FROM: " + lastSCN + ";TO: " + nextSCN);
                    lastSCN = nextSCN;
                }
            } else {
                if (currentSCN - lastSCN > 100) {
                    nextSCN = currentSCN;
                    System.out.println("FROM: " + lastSCN + ";TO: " + nextSCN);
                    lastSCN = nextSCN;
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

//        Config config = new Config();
//        config.setBuildType(LogMiner.LOG_TYPE.ONLINE);
//        config.setDictType(LogMiner.DICT_TYPE.ONLINE);
//        config.setCommittedDataOnly(true);
//        config.setNoRowid(true);
//        config.setContinuousMine(true);
//        config.setStartSCN(lastSCN + "");
//        config.setEndSCN("2302198");
//        config.setHasDictionary(true);
//        config.setDictionaryPath("D:\\app");

//        LogMiner miner = LogMinerFactory.createLogMiner(sourceDatabase, config);
//        miner.buildDictionary();
//        miner.build();
//        miner.start();
//        String lastScn   = "0";
//        String operation = null;
//        String sqlz      = null;
//        do{
//            String sql = "SELECT scn,operation,timestamp,status,sql_redo FROM v$logmnr_contents WHERE seg_owner='SCOTT' AND seg_type_name='TABLE' AND operation !='SELECT_FOR_UPDATE'";
//            try (ResultSet result = miner.query(sql)) {
//                System.out.println("Querying redo log:");
////            int max = 1000;
////            while (result.next() && max-- < 0) {
////                System.out.println(result.getString(1));
////            }
//
//                while (result.next()) {
//                    System.out.println("===> " + result.getObject(1) + "==");
//                    if (lastScn.equals(result.getObject(1))) {
//                        continue;
//                    }
//                    lastScn = String.valueOf(result.getObject(1));
//                    operation = result.getObject(2) + "";
//                    sqlz = result.getObject(5) + "";
//                    System.out.println("scn=" + lastScn + ",sql==" + sqlz + "");
//                    extractInsert(sqlz);
//                }
//                Thread.sleep(100);
//            } catch (Exception ex) {
//                assertTrue("Fail to query log miner result. Error message:" + ex.getMessage(), false);
//            }
//        }while(Integer.valueOf(lastScn) > currentSCN);
    }

    private void extractInsert(String text) {
        String table = text.substring("insert into ".length(), text.indexOf("("));
        table = table.replace("\"", "");
        Matcher  m     = Pattern.compile("\\((.*?)\\)").matcher(text);
        String[] cols  = null;
        String[] vals  = null;
        int      index = 0;
        while (m.find()) {
            if (index == 0) {
                String column = m.group(1);
                column = column.replaceAll("[^a-zA-Z0-9,_]+", "");
                cols = column.split(",");
            }
            if (index == 1) {
                String value = m.group(1);
                value = value.replaceAll("[\\s']+", "");
                vals = value.split(",");
            }
            index++;
        }
        System.out.println(table);
        for (int i = 0; i < cols.length; i++) {
            System.out.println("\t" + cols[i] + " = " + vals[i]);
        }
    }
}
